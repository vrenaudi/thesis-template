Template for writing a thesis.

This project is divided in the following folders:
- bib for .bib files
- figures for all the figures, a placeholder.png file (just a black rectangle) is also included as
    a tool to test the layout
- tex where all the tex files are located and the Makefile.

The latex compilation is taken care by latexmk, which needs to be installed
unless one wants to modify the Makefile.

Three commmands are set:
- `make` to perform the compilation
- `make clean` to remove all the temporary files but keeping the output pdf
- `make cleanall` which remove both temporary files and the output pdf

`main.tex` is the skeleton of the document where tex files can be included for
each part of the document (chapters, appendices, ...)

The source for the title page is not included as it often needs to follow
university guidelines.


